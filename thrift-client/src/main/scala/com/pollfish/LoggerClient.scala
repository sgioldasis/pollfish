package com.pollfish

import _root_.thrift.LoggerService.Log
import _root_.thrift.{LoggerService, Message}
import com.twitter.conversions.time._
import com.twitter.finagle._
import com.twitter.finagle.service._
import com.twitter.finagle.thrift.ThriftServiceIface
import com.twitter.finagle.util.HashedWheelTimer
import com.twitter.util._
import com.typesafe.config.ConfigFactory
import common.utils.logging.Logging
import common.utils.thrift.Helper._

/**
  * Created by savas on 30/09/16.
  */
object LoggerClient extends App with Logging {

  val conf = ConfigFactory.load.getConfig("logger")
  val server = conf.getString("server")

  val clientServiceIface: LoggerService.ServiceIface = Thrift.client
    .withSessionQualifier.noFailFast
    .newServiceIface[LoggerService.ServiceIface](server, "LoggerServiceInterface")

  val retryPolicy = RetryPolicy.tries[Try[Log.Result]](30000, {
    case Throw(ex: Exception) => clog.warn("Retrying ..."); true
  })

  val retriedLog =
    new RetryExceptionsFilter(retryPolicy, HashedWheelTimer(10.seconds, 1)) andThen
      ThriftServiceIface.resultFilter(Log) andThen
      clientServiceIface.log

  val client = retriedLog

  // Generate and send a number of messages defined by the argument (default 10)
  val argNumberOfMessages = Try(args(0).toInt).getOrElse(10)
  val requestList = Array.ofDim[Future[String]](argNumberOfMessages)
  for (i <- 0 until argNumberOfMessages) {

    val sentence = fabricator.Words().sentence(10)

    val message = Message(
      appCode = "LoggerClient",
      ip = localIpAddress,
      time = currentTimeUuid,
      logLevel = 1,
      m = sentence
    )

    clog.info(s"Sending sentence : $sentence")
    requestList(i) = client(Log.Args(message))

  }

  // Wait until all responses have been received
  val responseList = Await.result(Future.collect(requestList))
  responseList.foreach(clog.info)

  // Exit
  System.exit(0)

}
