package com.pollfish

import java.lang

import cakesolutions.kafka.KafkaConsumer
import common.utils.cassandra.Helper
import common.utils.cassandra.Helper._
import common.utils.kafka.Helper._
import common.utils.logging.Logging
import common.utils.retry.Retry

import scala.collection.JavaConversions._

/**
  * Created by savas on 04/10/16.
  */
object LoggerConsumer extends App with Logging with Retry {

  clog.info("Starting ...")

  // Cassandra session setup
  val session = Helper.createSessionAndInitKeyspace(cassandraUri)
  val stmt =
    """
     insert into messages
     (v,time,m,log_level,language,ip,app_code)
     values
     (?,?,?,?,?,?,?)
    ;
    """
  val prepared = session.prepare(stmt)

  // Kafka Consumer setup
  val consumer = KafkaConsumer(consumerConfig)
  consumer.subscribe(List(topic))

  clog.info("Waiting for messages ...")
  // Consumer loop
  while (true) {

    val records = consumer.poll(1000)
    val it = records.iterator()
    while (it.hasNext) {
      val rec = it.next
      val (key, msg) = (rec.key, rec.value)

      retry(1000) {
        val bound = prepared.bind(msg.v: lang.Short, java.util.UUID.fromString(msg.time), msg.m,
          msg.logLevel: lang.Byte, msg.language, msg.ip , msg.appCode)
        session.execute(bound)
        clog.info(s"Inserted : $msg")
      }
    }
  }

}
