import sbt.Keys._
import sbt.{ExclusionRule, Resolver}

name := "Pollfish Project"

scalaVersion := "2.11.8"


val commonSettings = Seq(
  organization := "com.pollfish",
  version := "0.1",
  scalaVersion := "2.11.8",
  scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8"),
  dependencyOverrides += "org.apache.thrift" % "libthrift" % "0.9.3",
  dependencyOverrides += "io.netty" % "netty-handler" % "4.0.37.Final",
  dependencyOverrides += "com.datastax.cassandra" % "cassandra-driver-core" % "3.1.0",
  dependencyOverrides += "com.typesafe" % "config" % "1.3.0",
  dependencyOverrides += "org.slf4j" % "slf4j-api" % "1.7.21",
  dependencyOverrides += "org.scala-lang" % "scala-library" % "2.11.8",
  dependencyOverrides += "commons-codec" % "commons-codec" % "1.9",
  dependencyOverrides += "org.scala-lang.modules" %% "scala-xml" % "1.0.5",
  updateOptions := updateOptions.value.withCachedResolution(true),
  fork := true,
  javaOptions ++= Seq("-Dlogback.configurationFile=src/test/resources/logback.xml"),
  resolvers += Resolver.bintrayRepo("cakesolutions", "maven"),
  resolvers += "Fabricator" at "http://dl.bintray.com/biercoff/Fabricator",
  resolvers += "twttr" at "https://maven.twttr.com/",
  resolvers += "github.release.repo" at "https://raw.github.com/bulldog2011/bulldog-repo/master/repo/releases/",
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case "application.conf" => MergeStrategy.discard
    case "io.netty.versions.properties" => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith ".properties" => MergeStrategy.first
    case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
    case x => MergeStrategy.first
  }
)

lazy val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.0.0" % "test"
    excludeAll(
    ExclusionRule(organization = "org.scala-lang.modules"),
    ExclusionRule("scala-xml", "scala-xml")
    )
  ,
  "org.scala-lang.modules" %% "scala-xml" % "1.0.5",
  "ch.qos.logback" % "logback-classic" % "1.0.9"
)

lazy val cassandraDependencies = Seq(
  "com.datastax.cassandra" % "cassandra-driver-core" % "3.1.0"
    excludeAll ExclusionRule(organization = "io.netty")
)

lazy val pillarDependencies = Seq(
  "com.chrisomeara" % "pillar_2.11" % "2.3.0"
    excludeAll(
    ExclusionRule(organization = "org.scala-lang"),
    ExclusionRule("jline", "jline")
    )
)

lazy val twitterDependencies = Seq(
  "com.twitter" %% "twitter-server" % "1.23.0"
)

lazy val kafkaDependencies = Seq(
  "net.cakesolutions" %% "scala-kafka-client" % "0.10.0.0"
    excludeAll(
    ExclusionRule(organization = "com.typesafe"),
    ExclusionRule(organization = "org.slf4j"),
    ExclusionRule(organization = "org.scala-lang")
    )
)

lazy val fabricatorDependencies = Seq(
  "com.github.azakordonets" % "fabricator_2.11" % "2.1.1"
    exclude("org.scala-lang", "scala-reflect")
)

lazy val giltDependencies = Seq(
  "com.gilt" %% "gfc-timeuuid" % "0.0.6"
)

lazy val configDependencies = Seq(
  "com.github.kxbmap" %% "configs" % "0.4.3"
)


lazy val common = project.in(file("common"))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= (
      testDependencies ++
        cassandraDependencies ++
        pillarDependencies ++
        twitterDependencies ++
        kafkaDependencies ++
        giltDependencies ++
        configDependencies
      )
  )

lazy val thriftServer = Project(
  id = "thrift-server",
  base = file("thrift-server"))
  .dependsOn(common % "compile")
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= (
      testDependencies ++
        twitterDependencies
      )
  )

lazy val thriftClient = Project(
  id = "thrift-client",
  base = file("thrift-client"))
  .dependsOn(common % "compile")
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= (
      testDependencies ++
        twitterDependencies ++
        fabricatorDependencies
      )
  )

lazy val kafkaConsumer = Project(
  id = "kafka-consumer",
  base = file("kafka-consumer"))
  .dependsOn(common % "compile")
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= (
      testDependencies ++
        twitterDependencies
      )
  )

lazy val main = project.in(file("."))
  .aggregate(common, thriftServer, thriftClient, kafkaConsumer)

