Pollfish Project
================

The project implements an aggregation/ingestion system for logging events.
There is a client that sends randomly generated logging events to a server
that accepts the events and pushes them on to Kafka. Another server consumes
the log events and writes them to a Cassandra database.

##Software versions

-   Scala: 2.11.8
-   JDK: 1.8
-   Thrift: 0.9.3
-   Kafka: 0.10.0.1
-   Cassandra: 3.9
-   Shell: Bash 4
   
##Pre-Requisites

- install latest java for your platform
- install Docker Engine [https://docs.docker.com/engine/installation/](https://docs.docker.com/engine/installation/)
- install Docker Compose [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

##Usage

Clone source code locally:

- ```git clone https://sgioldasis@bitbucket.org/sgioldasis/pollfish.git ```

Change into project directory:

- ```cd pollfish ```

Build the project:

- ```bin/sbt assembly ```

Start the infrastructure:

- ```bin/start ```

Check status of infrastructure (everything must be "Up" in "State" column):

- ```bin/status ```

Run tests: 

- ```bin/sbt test ```

Start kafka-consumer in one terminal (terminal 1):

- ```bin/kafka-consumer ```

Start thrift-server in a second terminal (terminal 2):

- ```bin/thrift-server ```

Start thrift-client in a third terminal (terminal 3):

 
- ```bin/thrift-client ``` Send 10 messages (default)

- ```bin/thrift-client 100``` Send 100 messages

To bring down any component, press Ctrl-C in the component's terminal.

##Verification

When you start thrift-client without an argument it sends 10 messages.
You can verify in the thrift-client terminal that the messages were
sent to the server and that the server processed them and responded.
You can also check outputs of thrift-server and kafka-consumer
(in their corresponding terminals) to verify that each received
and processed the messages. 

Once the messages are processed by kafka-consumer you can also verify
that they are indeed loaded in Cassandra:

- ```bin/cql ```

```
use pollfish;
select * from messages;
exit
```

##Failure Scenarios

In order to test failures you can bring down individual components of 
the infrastructure:

- ```bin/down kafka ```
- ```bin/down cassandra ```

To bring them back up:

- ```bin/up kafka ```
- ```bin/up cassandra ```

At any point you can check the status of infrastructure components:

- ```bin/status ```

Finally to bring down the whole infrastructure and release resources:

- ```bin/stop ```
