#!/bin/bash

#export MYIP=`ip route get 1 | awk '{print $NF;exit}'`

echo Starting ...
#export MYIP=$1

export MYIP=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | grep -v '172.'`
echo Set MYIP to $MYIP

# echo $MYIP
docker-compose up -d

echo Waiting for infrastructure to initialize ...
sleep 30
