/**
  * Created by savas on 29/09/16.
  */

package com.pollfish

import com.twitter.finagle.Thrift
import com.twitter.util.{Await, Future}
import cakesolutions.kafka._
import common.utils.kafka.Helper._
import common.utils.thrift.Helper._
import org.scalatest.{FunSpec, Matchers}
import thrift.{LoggerService, Message}
import scala.collection.JavaConversions._

class LoggerServerSpec extends FunSpec with Matchers {

  describe("A Logger Server") {
    it("should accept a message, write it to Kafka and respond") {

      // Initialize a producer
      val producer = KafkaProducer(producerConfig)
      // Initialize a consumer
      val consumer = KafkaConsumer(consumerConfig)
      consumer.subscribe(List(topic))

      // Send a message to initialize topic/producer/consumer
      producer.send(KafkaProducerRecord(topic, Some(currentTimeUuid),
        Message(appCode = "LoggerClient",
          ip = localIpAddress,
          time = currentTimeUuid,
          v = 4,
          logLevel = 1,
          m = "Initialize message !!!")
      ))
      producer.flush()

      // Start test logic
      def checkKafka() = {
        val records = consumer.poll(100)
        val it = records.iterator()
        while (it.hasNext) {
          val rec = it.next
          val key = rec.key
          val value = rec.value
          println(s"Retrieved from Kafka: Key [$key] Value [$value]")
        }
      }

      // Initialize a server and a client
      val service = Thrift.server.serveIface("localhost:1234", new LoggerServiceImpl(producer))
      val client = Thrift.client.newIface[LoggerService[Future]]("localhost:1234")

      // Send one
      val m2 = "aaaaa1"
      val r2 = Await.result(client.log(
          Message(appCode = "LoggerClient",
          ip = localIpAddress,
          time = currentTimeUuid,
          v = 4,
          logLevel = 1,
          m = m2)
      ))
      r2 should include(m2)
      checkKafka()

      // Send another one
      val m3 = "bbbbb2"
      val r3 = Await.result(client.log(
          Message(appCode = "LoggerClient",
          ip = localIpAddress,
          time = currentTimeUuid,
          v = 2,
          logLevel = 3,
          m = m3,
          language="el")
      ))
      r3 should include(m3)
      checkKafka()

      // Cleanup
      producer.close()
      consumer.close()

    }
  }
}
