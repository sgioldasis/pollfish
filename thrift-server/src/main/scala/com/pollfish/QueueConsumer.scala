package com.pollfish

import cakesolutions.kafka.{KafkaProducer, KafkaProducerRecord}
import com.leansoft.bigqueue.BigQueueImpl
import common.utils.kafka.Helper._
import common.utils.logging.Logging
import org.apache.kafka.clients.producer.RecordMetadata
import thrift.Message

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
  * Created by savas on 07/10/16.
  */
class QueueConsumer(producer: KafkaProducer[String, Message], queue: BigQueueImpl)
  extends Runnable with Logging {

  def run() = {

    while (true) {
      if (!queue.isEmpty) {
        val message = deserializer(queue.dequeue())

        clog.info(s"Sending message [$message] to topic [$topic]")
        val fResponse: Future[RecordMetadata] = producer.send(KafkaProducerRecord(topic, Some(message.time), message))

        try {
          Await.result(fResponse, 120 minutes)
        } catch {
          case ex: Exception => queue.enqueue(serializer(message))
        }
      }
    }

  }

}
