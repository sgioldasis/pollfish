package com.pollfish

import cakesolutions.kafka.KafkaProducer
import com.twitter.finagle.Thrift
import com.twitter.server.TwitterServer
import com.twitter.util.Await
import common.utils.kafka.Helper._

/**
  * Created by savas on 05/10/16.
  */
object LoggerServer extends TwitterServer {

  val producer = KafkaProducer(producerConfig)

  def main() {
    val server = Thrift.server.serveIface("localhost:1234", new LoggerServiceImpl(producer))
    onExit {
      server.close()
    }
    Await.ready(server)
  }

}
