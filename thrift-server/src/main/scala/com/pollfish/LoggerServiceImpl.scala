package com.pollfish

import java.util.concurrent.LinkedBlockingQueue

import cakesolutions.kafka.KafkaProducer
import com.leansoft.bigqueue.BigQueueImpl
import com.twitter.util.Future
import common.utils.logging.Logging
import thrift.{LoggerService, Message}
import common.utils.kafka.Helper._

/**
  * Created by savas on 30/09/16.
  */
class LoggerServiceImpl(producer: KafkaProducer[String, Message]) extends LoggerService[Future] with Logging {

  val bigQueue: BigQueueImpl = new BigQueueImpl("bigqueue", "messages")

  val queue = new LinkedBlockingQueue[Message]()
  val qconsumer = new QueueConsumer(producer, bigQueue)
  val startqconsumer = new Thread(qconsumer).start()

  def log(message: Message): Future[String] = {
    bigQueue.enqueue(serializer(message))
    Future.value(s"Server processed : ${message.m}")
  }

}




