/**
  * Created by savas on 29/09/16.
  */

package common.utils.cassandra

import com.datastax.driver.core.querybuilder.QueryBuilder
import com.datastax.driver.core.querybuilder.QueryBuilder._
import org.scalatest.{Matchers, FunSpec}

class ConnectionAndQuerySpec extends FunSpec with Matchers {

  describe("A Cassandra database") {

    it("should connect and execute queries") {
      val uri = CassandraConnectionUri("cassandra://localhost:9042/test")
      val session = Helper.createSessionAndInitKeyspace(uri)

      session.execute(
        " INSERT INTO messages " +
        " (v,time,m,log_level,language,ip,app_code) " +
        " VALUES " +
        " (1,e7125560-8a1a-11e6-910e-531a763e88a4,'hello again my beautiful server',1,'en','10.16.4.123','a1')" +
        ";"
      )

      val selectStmt = select().column("m")
        .from("messages")
        .where(QueryBuilder.eq("ip", "10.16.4.123"))
        .limit(1)

      val resultSet = session.execute(selectStmt)
      val row = resultSet.one()
      row.getString("m") should be("hello again my beautiful server")
      session.execute("TRUNCATE messages;")
    }
  }

}
