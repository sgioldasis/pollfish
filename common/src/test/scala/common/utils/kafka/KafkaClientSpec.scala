/**
  * Created by savas on 29/09/16.
  */

package common.utils.kafka

import cakesolutions.kafka._
import common.utils.kafka.Helper._
import common.utils.thrift.Helper._
import org.apache.kafka.clients.consumer.OffsetResetStrategy
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.scalatest.{FunSpec, Matchers}
import thrift.Message

import scala.collection.JavaConversions._


class KafkaClientSpec extends FunSpec with Matchers {

  val topic = "KafkaConsumerSpec2"

  // Initialize a producer
  val producerConfig = KafkaProducer.Conf(
    new StringSerializer(),
    KafkaSerializer(serializer),
    bootstrapServers = "localhost:9092",
    acks = "all",
    retries = 500,
    batchSize = 0
  )

  val producer = KafkaProducer(producerConfig)

  // Initialize a consumer
  val consumerConfig = KafkaConsumer.Conf(
    new StringDeserializer(),
    KafkaDeserializer(deserializer),
    bootstrapServers = "localhost:9092",
    groupId = "group-KafkaConsumerSpec",
    enableAutoCommit = false,
    autoCommitInterval = 1000,
    sessionTimeoutMs = 30000,
    autoOffsetReset = OffsetResetStrategy.EARLIEST
  )

  val consumer = KafkaConsumer(consumerConfig)
  consumer.subscribe(List(topic))


  describe("A Kafka Client") {
    it("should allow producing and consuming a message") {

      // Start test logic
      val msgTime = currentTimeUuid
      producer.send(KafkaProducerRecord(topic, Some(msgTime),
        Message(appCode = "KafkaConsumerSpec",
          ip = localIpAddress,
          time = msgTime,
          v = 4,
          logLevel = 1,
          m = "This is the text of the message")
      ))
      producer.flush()

      val records2 = consumer.poll(5000)
      records2.count() should be > 0

      println
      val it = records2.iterator()
      while (it.hasNext) {
        val rec = it.next
        val key = rec.key
        val value = rec.value
        println(s"Retrieved from Kafka: Topic [$topic] Key [$key] Value [$value]")
      }

      // Cleanup
      producer.close()
      consumer.close()

    }
  }

}
