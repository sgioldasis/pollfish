package common.utils.thrift

import java.util
import java.util.{Calendar, TimeZone}

import com.gilt.timeuuid._
import org.apache.thrift.protocol.TCompactProtocol
import org.apache.thrift.transport.{TMemoryBuffer, TMemoryInputTransport}
import org.jboss.netty.buffer.{ChannelBuffer, ChannelBuffers}
import thrift.{Message, TimeStamp}
import java.net._

/**
  * Created by savas on 03/10/16.
  */
object Helper {

  def currentTimestamp = {

    val now = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

    TimeStamp(
      year = now.get(Calendar.YEAR).asInstanceOf[Short],
      month = now.get(Calendar.MONTH).asInstanceOf[Byte],
      day = now.get(Calendar.DAY_OF_MONTH).asInstanceOf[Byte],
      hour = now.get(Calendar.HOUR_OF_DAY).asInstanceOf[Byte],
      minute = now.get(Calendar.MINUTE).asInstanceOf[Byte],
      second = now.get(Calendar.SECOND).asInstanceOf[Short],
      fraction = now.get(Calendar.MILLISECOND)
    )

  }

  def currentTimeUuid = TimeUuid().toString

  val localIpAddress = {

    var localIp = ""
    val e = NetworkInterface.getNetworkInterfaces
    while(e.hasMoreElements)
    {
      val n = e.nextElement match {
        case e: NetworkInterface => e
//        case _ => ???
      }

      val ee = n.getInetAddresses
      while (ee.hasMoreElements) {
        ee.nextElement match {
          case e: InetAddress  =>
            if (!e.getHostAddress.contains(":") && !e.getHostAddress.contains("127.0.0.1")) localIp = e.getHostAddress
//          case _ => ???
        }
      }
    }
    localIp

  }

  //  val protocolFactory = new TBinaryProtocol.Factory
  val protocolFactory = new TCompactProtocol.Factory

  final def toChannelBuffer(t: Message): ChannelBuffer = {
    //TMemoryBuffer allocation begins at 32 and grows based on need.
    val buf = new TMemoryBuffer(32)
    val oprot = protocolFactory.getProtocol(buf)
    Message.encode(t, oprot)
    ChannelBuffers.copiedBuffer(util.Arrays.copyOfRange(buf.getArray, 0, buf.length))
  }


  final def fromChannelBuffer(buffer: ChannelBuffer): Message = {
    val buf = new TMemoryInputTransport(buffer.array())
    val oprot = protocolFactory.getProtocol(buf)
    Message.decode(oprot)
  }

  final def fromByteArray(buffer: Array[Byte]): Message = {
    val buf = new TMemoryInputTransport(buffer)
    val oprot = protocolFactory.getProtocol(buf)
    Message.decode(oprot)
  }



}
