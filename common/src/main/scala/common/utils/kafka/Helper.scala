package common.utils.kafka

import cakesolutions.kafka.{KafkaConsumer, KafkaDeserializer, KafkaProducer, KafkaSerializer}
import com.typesafe.config.ConfigFactory
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import thrift.Message

/**
  * Created by savas on 03/10/16.
  */
object Helper {

  val conf = ConfigFactory.load.getConfig("kafka")
  val confConsumer = conf.getConfig("consumer")
  val confProducer = conf.getConfig("producer")
  val topic = conf.getString("topic")

  val serializer = (msg: Message) => common.utils.thrift.Helper.toChannelBuffer(msg).array()
  val deserializer = (bytes: Array[Byte]) => common.utils.thrift.Helper.fromByteArray(bytes)

  val consumerConfig: KafkaConsumer.Conf[String, Message] = {
    KafkaConsumer.Conf(confConsumer,
      new StringDeserializer(),
      KafkaDeserializer(deserializer)
    )
  }

  val producerConfig: KafkaProducer.Conf[String, Message] = {
    KafkaProducer.Conf(confProducer,
      new StringSerializer(),
      KafkaSerializer(serializer)
    )
  }

}
