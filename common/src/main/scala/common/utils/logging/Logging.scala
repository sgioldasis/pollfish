package common.utils.logging

import org.slf4j.LoggerFactory

/**
  * Created by savas on 07/10/16.
  */
trait Logging {
  val clog = LoggerFactory.getLogger(getClass)
}
