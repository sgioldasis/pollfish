package common.utils.retry

import common.utils.logging.Logging

/**
  * Created by savas on 8/10/2016.
  */
trait Retry extends Logging {
  @annotation.tailrec
  final def retry[T](n: Int)(fn: => T): T = {
    util.Try { fn } match {
      case util.Success(x) => x
      case _ if n > 1 => { clog.info("Retrying..."); retry(n - 1)(fn)}
      case util.Failure(e) => throw e
    }
  }
}
