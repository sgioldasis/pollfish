/**
  * Created by savas on 29/09/16.
  */
package common.utils.cassandra

import com.datastax.driver.core._
import com.typesafe.config.ConfigFactory

//
object Helper {

  val conf = ConfigFactory.load.getConfig("cassandra")
  val cassandraUri = CassandraConnectionUri(conf.getString("uri"))

  def createSessionAndInitKeyspace(uri: CassandraConnectionUri,
                                   defaultConsistencyLevel: ConsistencyLevel = QueryOptions.DEFAULT_CONSISTENCY_LEVEL
                                  ) = {
    val cluster = new Cluster.Builder().
      addContactPoints(uri.hosts.toArray: _*).
      withPort(uri.port).
      withQueryOptions(new QueryOptions().setConsistencyLevel(defaultConsistencyLevel)).build

    val session = cluster.connect

    session.execute(
      s"CREATE KEYSPACE IF NOT EXISTS ${uri.keyspace} " +
      s"WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': 1 };")
    session.execute(s"USE ${uri.keyspace}")

    Pillar.initialize(session, uri.keyspace, 1)
    Pillar.migrate(session)

    session
  }

}
