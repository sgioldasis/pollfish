struct TimeStamp {
  1: i16 year
  2: byte month
  3: byte day
  4: byte hour=0
  5: byte minute=0
  6: i16 second=0
  7: double fraction=0
}

struct Message {
    1: required i16 v = 1;
    2: required string time;
    3: required string m;
    4: required byte logLevel;
    5: required string language = "en"
    6: required string ip;
    7: required string appCode;
}

service LoggerService {
  string log(1: Message message);
}
